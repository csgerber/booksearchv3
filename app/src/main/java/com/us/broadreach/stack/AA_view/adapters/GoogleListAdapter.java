package com.us.broadreach.stack.AA_view.adapters;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.navigation.Navigation;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.us.broadreach.stack.CC_model.cache.Cache;
import com.us.broadreach.stack.CC_model.models.Item;
import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import com.us.broadreach.stack.CC_model.utils.Constants;
import com.us.broadreach.stack.CC_model.utils.LoadState;
import com.us.broadreach.stack.R;


public class GoogleListAdapter extends PagedListAdapter<Item, RecyclerView.ViewHolder> {

    private final int DATA_VIEW_TYPE = 1;
    private final int FOOTER_VIEW_TYPE = 2;

    private RetryCallback retry;
    private LoadState loadState = LoadState.LOADING;

    public GoogleListAdapter(RetryCallback retryCallback) {
        super(itemDiffCallback);
        this.retry = retryCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == DATA_VIEW_TYPE)
            return GoogleBodyViewHolder.create(parent);
        else
            return GoogleFooterViewHolder.create(parent);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < super.getItemCount())
            return DATA_VIEW_TYPE;
        else
            return FOOTER_VIEW_TYPE;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            ((GoogleBodyViewHolder) holder).bind(getItem(position));
        else
            ((GoogleFooterViewHolder) holder).bind(loadState, retry);
    }

    @Override
    public int getItemCount() {
        if (hasFooter()) {
            return super.getItemCount() + 1;
        } else {
            return super.getItemCount();
        }
    }

    Boolean hasFooter() {
        return super.getItemCount() != 0 && (loadState == LoadState.LOADING || loadState == LoadState.ERROR);
    }

    public void setLoadState(LoadState loadState) {
        this.loadState = loadState;
        notifyItemChanged(super.getItemCount());
    }

    //if the title
    public static final DiffUtil.ItemCallback<Item> itemDiffCallback =
            new DiffUtil.ItemCallback<Item>() {
                @Override
                public boolean areItemsTheSame(
                        @NonNull Item oldItem, @NonNull Item newItem) {
                    // User properties may have changed if reloaded from the DB, but ID is fixed
                    if(oldItem!=null && oldItem.getVolumeInfo() != null && oldItem.getVolumeInfo().getTitle() != null &&
                            newItem!=null && newItem.getVolumeInfo() != null && newItem.getVolumeInfo().getTitle() != null)
                        return oldItem.getVolumeInfo().getTitle().equals(newItem.getVolumeInfo().getTitle());
                    else
                        return true;
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(
                        @NonNull Item oldItem, @NonNull Item newItem) {
                    // NOTE: if you use equals, your object must properly override Object.equals()
                    // Incorrectly returning false here will result in too many animations.
                    if(oldItem!=null && newItem!=null)
                        return oldItem.equals(newItem);
                    else
                        return true;
                }

            };
}

//inner class
class GoogleBodyViewHolder extends RecyclerView.ViewHolder {
     ImageView newsBanner;
     TextView bookItemTitle;
     TextView bookItemAuthors, bookItemPublishedDate;

    //need to reference to the containerView in static context
    public static View containerView;


    GoogleBodyViewHolder(View itemView) {
        super(itemView);

        newsBanner =  itemView.findViewById(R.id.imgCover);
        bookItemTitle =  itemView.findViewById(R.id.txtBookTitle);
        bookItemAuthors =  itemView.findViewById(R.id.txtBookAuthor);
        bookItemPublishedDate =  itemView.findViewById(R.id.txtBookDate);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void bind(Item item) {
        if (item != null) {
            bookItemTitle.setText(item.getVolumeInfo().getTitle());
            if (null != item.getVolumeInfo() && null != item.getVolumeInfo().getImageLinks() && null != item.getVolumeInfo().getImageLinks().getSmallThumbnail()) {

                try {
                    Glide.with(containerView.getContext())
                            .load(item.getVolumeInfo().getImageLinks().getSmallThumbnail())
                            .into(newsBanner);
                } catch (Exception e) {
                    e.getMessage();
                }

            }

            if (item.getVolumeInfo().getAuthors() != null) {
                String authors = String.join(", ", item.getVolumeInfo().getAuthors());
                bookItemAuthors.setText(authors);
            }

            bookItemPublishedDate.setText(item.getVolumeInfo().getPublishedDate());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bookVolumeData = new Bundle();
                    bookVolumeData.putSerializable(Constants.item, FavoriteItem.fromItem(item, Cache.getInstance().getUserEmail()));
                    bookVolumeData.putBoolean(Constants.favMode, false);


                    Navigation.findNavController(view).navigate(R.id.action_googleListFragment_to_detailFragment,bookVolumeData);



                }
            });
        }
    }



    public static GoogleBodyViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_google, parent, false);
        containerView = parent;
        return new GoogleBodyViewHolder(view);
    }


}


//inner class
class GoogleFooterViewHolder extends RecyclerView.ViewHolder {

    ProgressBar progressBar;
    TextView errorText;
    RetryCallback retry;

    GoogleFooterViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
        errorText = itemView.findViewById(R.id.txtError);

    }

    public void bind(LoadState status, RetryCallback retryCallback) {
        this.retry = retryCallback;
        if (status == LoadState.LOADING)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        if (status == LoadState.ERROR) {
            errorText.setVisibility(View.VISIBLE);
        } else {
            errorText.setVisibility(View.INVISIBLE);
        }

        errorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry.retry();
            }
        });
    }

    public static GoogleFooterViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_footer, parent, false);
        return new GoogleFooterViewHolder(view);
    }

}