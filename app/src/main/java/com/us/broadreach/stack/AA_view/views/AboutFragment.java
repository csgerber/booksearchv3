package com.us.broadreach.stack.AA_view.views;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.us.broadreach.stack.CC_model.cache.Cache;
import com.us.broadreach.stack.R;


public class AboutFragment extends Fragment {

    private TextView headerTitle;
    private EditText edtSubject, edtMessage;
    private Button btnSend, btnBack;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_about, container, false);

        headerTitle = containerView.findViewById(R.id.txtHeaderTitle);
        headerTitle.setText(R.string.about_this_app);
        headerTitle.setVisibility(View.VISIBLE);
        btnBack = containerView.findViewById(R.id.btnBack);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               getActivity().onBackPressed();
            }
        });
        edtMessage = containerView.findViewById(R.id.edtMessage);
        edtSubject = containerView.findViewById(R.id.edtSubject);
        btnSend = containerView.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(edtSubject.getText());
                stringBuilder.append("\n");
                stringBuilder.append(edtMessage.getText());
                stringBuilder.append("\n");
                stringBuilder.append(Cache.getInstance().getUserEmail());

                Toast.makeText(getActivity(), stringBuilder.toString(), Toast.LENGTH_LONG).show();
            }
        });

        return containerView;
    }
}