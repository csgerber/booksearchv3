package com.us.broadreach.stack.CC_model.utils;

public enum LoadState {
    DONE, LOADING, ERROR, NODATA
}
