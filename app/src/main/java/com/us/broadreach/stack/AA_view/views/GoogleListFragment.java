package com.us.broadreach.stack.AA_view.views;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import com.amazonaws.mobile.client.AWSMobileClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import com.us.broadreach.stack.AA_view.adapters.GoogleListAdapter;
import com.us.broadreach.stack.AA_view.adapters.RetryCallback;
import com.us.broadreach.stack.BB_viewmodel.viewmodels.GoogleListViewModel;
import com.us.broadreach.stack.CC_model.cache.Cache;
import com.us.broadreach.stack.CC_model.models.Item;
import com.us.broadreach.stack.CC_model.utils.LoadState;
import com.us.broadreach.stack.R;
import com.us.broadreach.stack.FavoritesApp;

public class GoogleListFragment extends Fragment {
    private TextView txtError, txtNoData;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private GoogleListViewModel viewModel = null;
    private GoogleListAdapter booksListAdapter = null;
    private TextView headerTitle;

    private Button btnList;
    private Toolbar toolbar;
    private String keyword;
    private EditText keywordEditText;

    private View containerView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        containerView = inflater.inflate(R.layout.frag_google_list, container, false);

        viewModel = new ViewModelProvider(this).get(GoogleListViewModel.class);
        txtError = containerView.findViewById(R.id.txtError);
        txtNoData = containerView.findViewById(R.id.txtNoData);

        toolbar = containerView.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main);
        Drawable drawable = toolbar.getOverflowIcon();
        if(drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), Color.WHITE );
            toolbar.setOverflowIcon(drawable);
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.about:
                        Navigation.findNavController(containerView).navigate(R.id.aboutFragment);
                        return true;
                    case R.id.logout:
                        if (Boolean.parseBoolean( getResources().getString(R.string.use_auth))){
                            AWSMobileClient.getInstance().signOut();
                            Intent intent = new Intent(FavoritesApp.getAppContext(), AuthActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return true;

                }
                return false;
            }
        });


        //toolbar
        headerTitle = containerView.findViewById(R.id.txtHeaderTitle);
        btnList = containerView.findViewById(R.id.btnList);
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.favoritesFragment);

            }
        });


        progressBar = containerView.findViewById(R.id.progressBar);
        recyclerView = containerView.findViewById(R.id.recyclerView);
        keywordEditText = containerView.findViewById(R.id.edtKeyword);
        keywordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    if (keyEvent.getAction() == KeyEvent.ACTION_UP) return true;

                    recyclerView.stopScroll();
                    recyclerView.getRecycledViewPool().clear();
                    keyword = keywordEditText.getEditableText().toString().trim();
                    Cache.getInstance().setKeyword(keyword);
                    //Emit to the EventBus to invalidate any previous observers to make way for new observers in the GooglePagedKeyedDataSource
                    EventBus.getDefault().post(keyword);
                    booksListAdapter.notifyDataSetChanged();
                    return true;
                }
                return false;

            }
        });
        keywordEditText.setText(Cache.getInstance().getKeyword());


        booksListAdapter = new GoogleListAdapter(retryCallback);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(booksListAdapter);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 10);
        recyclerView.setItemViewCacheSize(10);

        viewModel.getBooksList().observe(getActivity(), new Observer<PagedList<Item>>() {
            @Override
            public void onChanged(PagedList<Item> articles) {
                if (articles != null) {
                    booksListAdapter.submitList(articles);
                }
            }
        });


        txtError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.retry();
            }
        });

        viewModel.getState().observe(getActivity(), new Observer<LoadState>() {
            @Override
            public void onChanged(LoadState state) {
                MainActivity.hideKeyboardFrom(getContext(), GoogleListFragment.this.getView());
                recyclerView.setVisibility(View.VISIBLE);

                if (viewModel.listIsEmpty() && state == LoadState.LOADING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                if (viewModel.listIsEmpty() && state == LoadState.ERROR) {
                    txtError.setVisibility(View.VISIBLE);
                } else {
                    txtError.setVisibility(View.GONE);
                }


                if (state == LoadState.NODATA) {
                    recyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                }

                if (!viewModel.listIsEmpty()) {
                    if (state == null) {
                        booksListAdapter.setLoadState(LoadState.DONE);
                    } else {
                        booksListAdapter.setLoadState(state);
                    }
                }
                booksListAdapter.notifyDataSetChanged();

            }

        });

        return containerView;
    }


    RetryCallback retryCallback = new RetryCallback() {
        @Override
        public void retry() {
            viewModel.retry();
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        Log.d("USER_DETAILS", Cache.getInstance().getUserEmail());
        btnList.setVisibility(View.VISIBLE);
        headerTitle.setText(getResources().getString(R.string.book_search));

    }
}
