package com.us.broadreach.stack.AA_view.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import androidx.navigation.Navigation;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.us.broadreach.stack.CC_model.cache.Cache;
import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import com.us.broadreach.stack.CC_model.repositories.FavoriteRepository;
import com.us.broadreach.stack.CC_model.utils.Constants;
import com.us.broadreach.stack.FavoritesApp;


import com.us.broadreach.stack.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailFragment extends Fragment {


    private TextView authorName, email;
    private TextView title;
    private FavoriteItem favoriteItem;
    private TextView year;
    private TextView description;
    private TextView txtTitle;
    private ImageView imgBook;
    private FloatingActionButton fab;

    private boolean favMode;



    //toolbar
    private Button btnShare, btnAction, btnList,  btnBack;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        favMode =  getArguments().getBoolean(Constants.favMode, false);

        View view = inflater.inflate(R.layout.frag_detail, container, false);

        authorName = view.findViewById(R.id.txtAuthor);
        title = view.findViewById(R.id.txtTitle);
        year = view.findViewById(R.id.txtYear);
        description = view.findViewById(R.id.txtDesc);
        imgBook = view.findViewById(R.id.imgBook);
        email = view.findViewById(R.id.txtEmail);




        //toolbar elements
        btnShare = view.findViewById(R.id.btnShare);
        btnList = view.findViewById(R.id.btnList);
        btnAction = view.findViewById(R.id.btnAction);

        //fab
        fab = view.findViewById(R.id.fab);


        btnBack = view.findViewById(R.id.btnBack);
        txtTitle = view.findViewById(R.id.txtHeaderTitle);

        //set title bar elements
        btnBack.setVisibility(View.VISIBLE);
        btnList.setVisibility(View.GONE);

        //get the data from the args and set to the fields of this frag
        fetchDataFromBundleArguments(getArguments());




        if (!favMode) {

          //  btnAction.setBackground(ContextCompat.getDrawable(FavoritesApp.getAppContext(), R.drawable.ic_add));
            txtTitle.setText("Book Detail");
            view.setBackgroundColor(ContextCompat.getColor(FavoritesApp.getAppContext(), R.color.white));
            btnShare.setVisibility(View.GONE);
            email.setVisibility(View.GONE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    favoriteItem.setUserEmail(Cache.getInstance().getUserEmail());
                    Call<FavoriteItem> call = FavoriteRepository.getService().addOne(favoriteItem);
                    call.enqueue(new Callback<FavoriteItem>() {
                        @Override
                        public void onResponse(Call<FavoriteItem> call, Response<FavoriteItem> response) {
                            System.out.println(response);
                            switch (response.code()){
                                case 200:
                                    Bundle bundle = new Bundle();
                                    bundle.putString(Constants.action, getString(R.string.fav_added));
                                    bundle.putSerializable(Constants.item, favoriteItem);
                                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment,bundle);
                                    break;

                                case 415:
                                    Toast.makeText(FavoritesApp.getAppContext(), getResources().getString(R.string.fav_already), Toast.LENGTH_LONG).show();
                                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment);
                                    break;
                                default:
                                    Toast.makeText(FavoritesApp.getAppContext(), getResources().getString(R.string.was_error) + response.code(), Toast.LENGTH_LONG).show();
                                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment);
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<FavoriteItem> call, Throwable t) {

                            Toast.makeText(FavoritesApp.getAppContext(), getResources().getString(R.string.was_error) + t.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    });
                }
            });

        }
        else {
            btnAction.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
            txtTitle.setText(R.string.favorite_detail);
            btnAction.setBackground(ContextCompat.getDrawable(FavoritesApp.getAppContext(), R.drawable.ic_rubish));
            view.setBackgroundColor(ContextCompat.getColor(FavoritesApp.getAppContext(), R.color.light_yellow));
            btnShare.setVisibility(View.VISIBLE);
            email.setVisibility(View.VISIBLE);
            email.setText(favoriteItem.getUserEmail());
            btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Call<FavoriteItem> call = FavoriteRepository.getService().deleteById(favoriteItem.getUserEmail(), favoriteItem.getId() );
                    call.enqueue(new Callback<FavoriteItem>() {
                        @Override
                        public void onResponse(Call<FavoriteItem> call, Response<FavoriteItem> response) {
                            System.out.println(response);
                            switch (response.code()){
                                case 200:
                                    Bundle bundle = new Bundle();
                                    bundle.putString(Constants.action, getResources().getString(R.string.fav_removed));
                                    bundle.putSerializable(Constants.item, favoriteItem);
                                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment,bundle);
                                    break;
                                default:
                                    Toast.makeText(FavoritesApp.getAppContext(), getResources().getString(R.string.error_on_server) + response.code(), Toast.LENGTH_LONG).show();
                                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment);
                                    break;

                            }
                        }

                        @Override
                        public void onFailure(Call<FavoriteItem> call, Throwable t) {
                            String errorMsg = "Error found on not favorite is : " + t.getMessage();
                            System.out.println(errorMsg);
                        }
                    });


                }
            });
        }

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, title.getText().toString().trim() + "\n \n" + description.getText().toString().trim());
                startActivity(sendIntent);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.action, "no-action");
                if (favMode){
                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_favoitesFragment,bundle);
                } else {
                    Navigation.findNavController(view).navigate(R.id.action_detailFragment_to_googleListFragment,bundle);

                }


            }
        });
        return view;
    }



    private void fetchDataFromBundleArguments(Bundle bundle) {
        if (bundle != null) {
            favoriteItem = (FavoriteItem) bundle.getSerializable(Constants.item);

            title.setText(favoriteItem.getTitle());
            year.setText(favoriteItem.getYear());
            description.setText(favoriteItem.getDescription());
            authorName.setText(favoriteItem.getAuthorName());

            Glide.with(FavoritesApp.getAppContext())
                    .load(favoriteItem.getLink())
                    .into(imgBook);
        }
    }



}
