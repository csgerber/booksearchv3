package com.us.broadreach.stack.AA_view.adapters;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.navigation.Navigation;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.us.broadreach.stack.R;

import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import com.us.broadreach.stack.CC_model.utils.Constants;
import com.us.broadreach.stack.CC_model.utils.LoadState;



public class FavoriteAdapter extends PagedListAdapter<FavoriteItem, RecyclerView.ViewHolder> {

    private final int DATA_VIEW_TYPE = 1;
    private final int FOOTER_VIEW_TYPE = 2;

    private RetryCallback retry;
    private LoadState loadState = LoadState.LOADING;

    public FavoriteAdapter(RetryCallback retryCallback) {
        super(favDiffCallback);
        this.retry = retryCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == DATA_VIEW_TYPE)
            return FavBodyViewHolder.create(parent);
        else
            return FavFooterViewHolder.create(parent);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < super.getItemCount())
            return DATA_VIEW_TYPE;
        else
            return FOOTER_VIEW_TYPE;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            ((FavBodyViewHolder) holder).bind(getItem(position));
        else
            ((FavFooterViewHolder) holder).bind(loadState, retry);
    }

    @Override
    public int getItemCount() {
        if (hasFooter()) {
            return super.getItemCount() + 1;
        } else {
            return super.getItemCount();
        }
    }

    Boolean hasFooter() {
        return super.getItemCount() != 0 && (loadState == LoadState.LOADING || loadState == LoadState.ERROR);
    }

    public void setLoadState(LoadState loadState) {
        this.loadState = loadState;
        notifyItemChanged(super.getItemCount());
    }

    public static final DiffUtil.ItemCallback<FavoriteItem> favDiffCallback =
            new DiffUtil.ItemCallback<FavoriteItem>() {
                @Override
                public boolean areItemsTheSame(
                        @NonNull FavoriteItem oldFav, @NonNull FavoriteItem newFav) {
                    // User properties may have changed if reloaded from the DB, but ID is fixed
                    if(oldFav!=null && oldFav.getId() != null &&
                      newFav!=null && newFav.getId() != null
                    )
                        return oldFav.getId().equals(newFav.getId());
                    else
                        return true;
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(
                        @NonNull FavoriteItem oldWish, @NonNull FavoriteItem newWish) {
                    // NOTE: if you use equals, your object must properly override Object#equals()
                    // Incorrectly returning false here will result in too many animations.
                    if(oldWish!=null && newWish!=null)
                        return oldWish.equals(newWish);
                    else
                        return true;
                }
            };
}

//inner class
class FavBodyViewHolder extends RecyclerView.ViewHolder {
     ImageView bookCover;
     TextView bookItemTitle;
     TextView bookItemAuthors, bookItemPublishedDate, bookItemEmail;

    //need to reference to the containerView to communicate with the operating system
    public static View containerView;


    FavBodyViewHolder(View itemView) {
        super(itemView);

        bookCover =  itemView.findViewById(R.id.imgCover);
        bookItemTitle =  itemView.findViewById(R.id.txtBookTitle);
        bookItemAuthors =  itemView.findViewById(R.id.txtBookAuthor);
        bookItemPublishedDate =  itemView.findViewById(R.id.txtBookDate);
        bookItemEmail =  itemView.findViewById(R.id.txtBookEmail);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void bind(FavoriteItem item) {
        if (item != null) {
            bookItemTitle.setText(item.getTitle());

                try {
                    Glide.with(containerView.getContext())
                            .load(item.getLink())
                            .into(bookCover);
                } catch (Exception e) {
                    e.getMessage();
                }



            if (item.getAuthorName() != null) {
                String authors = String.join(", ", item.getAuthorName());
                bookItemAuthors.setText(authors);
            }

            bookItemPublishedDate.setText(item.getYear());
            bookItemPublishedDate.setText(item.getUserEmail());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bookVolumeData = new Bundle();
                    bookVolumeData.putSerializable(Constants.item, item);
                    bookVolumeData.putBoolean(Constants.favMode, true);

                    Navigation.findNavController(view).navigate(R.id.action_favoritesFragment_to_detailFragment,bookVolumeData);



                }
            });
        }
    }



    public static FavBodyViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fav, parent, false);
        containerView = parent;
        return new FavBodyViewHolder(view);
    }


}


//inner class
class FavFooterViewHolder extends RecyclerView.ViewHolder {

    ProgressBar progressBar;
    TextView errorText;
    RetryCallback retry;

    FavFooterViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
        errorText = itemView.findViewById(R.id.txtError);

    }

    public void bind(LoadState status, RetryCallback retryCallback) {
        this.retry = retryCallback;
        if (status == LoadState.LOADING)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        if (status == LoadState.ERROR) {
            errorText.setVisibility(View.VISIBLE);
        } else {
            errorText.setVisibility(View.INVISIBLE);
        }

        errorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry.retry();
            }
        });
    }

    public static FavFooterViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_footer, parent, false);
        return new FavFooterViewHolder(view);
    }

}