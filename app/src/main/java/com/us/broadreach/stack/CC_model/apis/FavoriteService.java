package com.us.broadreach.stack.CC_model.apis;



import java.util.List;

import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FavoriteService {

    @GET("/wish/paged/{userEmail}/{page}")
    Single<List<FavoriteItem>> pagedWishes(
            @Path("userEmail") String userEmail,
            @Path("page") int page

    );


    @GET("/wish/{userEmail}/{id}")
    Call<FavoriteItem> getById(
            @Path("userEmail") String userEmail,
            @Path("id") String id

    );

    @POST("/wish")
    Call<FavoriteItem> addOne(
            @Body FavoriteItem item

    );

    @DELETE("/wish/{userEmail}/{id}")
    Call<FavoriteItem> deleteById (
            @Path("userEmail") String userEmail,
            @Path("id") String id
    );




}
