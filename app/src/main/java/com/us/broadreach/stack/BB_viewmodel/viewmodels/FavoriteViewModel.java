package com.us.broadreach.stack.BB_viewmodel.viewmodels;


import android.util.Log;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.us.broadreach.stack.CC_model.apis.FavoriteService;
import com.us.broadreach.stack.CC_model.cache.Cache;
import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import com.us.broadreach.stack.CC_model.repositories.FavoriteRepository;
import com.us.broadreach.stack.CC_model.utils.LoadState;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.schedulers.Schedulers;

public class FavoriteViewModel extends ViewModel {

    public LiveData<PagedList<FavoriteItem>> favoritesList;

    private final CompositeDisposable compositeDisposable;
    private final FavoriteDataSourceFactory favoriteDataSourceFactory;


    public FavoriteViewModel() {
        super();

        compositeDisposable = new CompositeDisposable();
        favoriteDataSourceFactory = new FavoriteDataSourceFactory(compositeDisposable, FavoriteRepository.getService());
        final int PAGE_SIZE = 20;
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setEnablePlaceholders(false)
                .build();

        favoritesList = new LivePagedListBuilder(favoriteDataSourceFactory, config).build();

    }

    public LiveData<LoadState> getState() {

        return Transformations.switchMap(
                favoriteDataSourceFactory.newsDataSourceLiveData,
                FavoritePagedKeyedDataSource::getState
        );
    }

    public LiveData<PagedList<FavoriteItem>> getBooksList() {
        return favoritesList;
    }


    public void retry() {
        favoriteDataSourceFactory.newsDataSourceLiveData.getValue().retry();
    }

    public Boolean listIsEmpty() {
        if (favoritesList.getValue() == null) {
            return true;
        } else {
            return favoritesList.getValue().isEmpty();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }


}

class FavoriteDataSourceFactory extends DataSource.Factory<Integer, FavoriteItem> {
    public MutableLiveData<FavoritePagedKeyedDataSource> newsDataSourceLiveData = new MutableLiveData<FavoritePagedKeyedDataSource>();
    public CompositeDisposable compositeDisposable;
    public FavoriteService favoriteService;

    public FavoriteDataSourceFactory(CompositeDisposable compositeDisposable, FavoriteService favoriteService) {
        this.compositeDisposable = compositeDisposable;
        this.favoriteService = favoriteService;
    }

    @NonNull
    @Override
    public DataSource<Integer, FavoriteItem> create() {
        FavoritePagedKeyedDataSource favoritesPagedKeyedDataSource = new FavoritePagedKeyedDataSource(favoriteService, compositeDisposable);
        newsDataSourceLiveData.postValue(favoritesPagedKeyedDataSource);
        return favoritesPagedKeyedDataSource;
    }
}


class FavoritePagedKeyedDataSource extends PageKeyedDataSource<Integer, FavoriteItem> {

    FavoriteService favoriteService;
    CompositeDisposable compositeDisposable;
    MutableLiveData<LoadState> state = new MutableLiveData<>();

    Completable retryCompletable = null;
    int page = 1;


    public MutableLiveData<LoadState> getState() {
        return state;
    }

    FavoritePagedKeyedDataSource(FavoriteService favoriteService,
                                 CompositeDisposable compositeDisposable) {
        super();

        EventBus.getDefault().register(this);
        this.favoriteService = favoriteService;
        this.compositeDisposable = compositeDisposable;
        updateState(LoadState.DONE);
    }

    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, FavoriteItem> callback) {
        updateState(LoadState.LOADING);

        Log.i("loadInitial", "loadInitial");
        Log.i("loadInitial-email", Cache.getInstance().getUserEmail());


        compositeDisposable.add(
                favoriteService.pagedWishes(Cache.getInstance().getUserEmail(), page)
                        .subscribe(new BiConsumer<List<FavoriteItem>, Throwable>() {
                            @Override
                            public void accept(List<FavoriteItem> response, Throwable throwable) throws Exception {
                                if (response != null) {
                                    if (response.size() == 0) {
                                        updateState(LoadState.NODATA);
                                    } else {
                                        Log.d("RESPONSE_CONSUME_LIA", response.get(0).getId() + "");
                                        updateState(LoadState.DONE);
                                        callback.onResult(response, null, page);
                                    }

                                } else {
                                    updateState(LoadState.ERROR);
                                    setRetry(new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            Log.d("RESPONSE_CONSUME_LIE", response + "");
                                            loadInitial(params, callback);
                                        }
                                    });
                                }

                            }
                        }));


    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, FavoriteItem> callback) {

    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, FavoriteItem> callback) {
        updateState(LoadState.LOADING);

        Log.i("loadAfter", "loadAfter");
        Log.i("loadAfter-email", Cache.getInstance().getUserEmail());

        page = page + 1;

        Log.d("PAGING", params.requestedLoadSize + ":" + params.key);

        compositeDisposable.add(
                favoriteService.pagedWishes(Cache.getInstance().getUserEmail(), page)
                        .subscribe(new BiConsumer<List<FavoriteItem>, Throwable>() {
                            @Override
                            public void accept(List<FavoriteItem> response, Throwable throwable) throws Exception {

                                if (response != null ) {
                                    if (response.size() > 0){
                                        callback.onResult(response, page);
                                    }
                                    Log.d("RESPONSE_CONSUME_LAA", params.key + "");
                                    updateState(LoadState.DONE);

                                } else {
                                    updateState(LoadState.ERROR);
                                    setRetry(new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            Log.d("RESPONSE_CONSUME_LAE", params.key + "");
                                            loadAfter(params, callback);
                                        }
                                    });
                                }

                            }
                        }));

    }

    private void updateState(LoadState loadState) {
        this.state.postValue(loadState);
        Log.d("EVENT_BUS_UPDATE_STATUS",   page + " " + loadState);

    }

    public void retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe());
        }
    }

    private void setRetry(Action action) {
        if (action == null) {
            retryCompletable = null;
        } else {
            retryCompletable = Completable.fromAction(action);
        }

    }

    //the IDE does not recognize this method, but the EventBus IS actually using it.
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String keyword) {

        invalidate();
        compositeDisposable.dispose();
    }

}
