package com.us.broadreach.stack.AA_view.views;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import com.us.broadreach.stack.AA_view.adapters.FavoriteAdapter;
import com.us.broadreach.stack.AA_view.adapters.RetryCallback;
import com.us.broadreach.stack.BB_viewmodel.viewmodels.FavoriteViewModel;
import com.us.broadreach.stack.CC_model.models.FavoriteItem;
import com.us.broadreach.stack.CC_model.utils.Constants;
import com.us.broadreach.stack.CC_model.utils.LoadState;
import com.us.broadreach.stack.R;
import com.us.broadreach.stack.FavoritesApp;

import java.util.Objects;


public class FavoriteListFragment extends Fragment {
    private TextView txtError, txtNoData;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private FavoriteViewModel viewModel = null;
    private FavoriteAdapter favoriteAdapter = null;
    private TextView headerTitle;
    private Button imgSearch;
    private Toolbar toolbar;
    private View containerView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        containerView = inflater.inflate(R.layout.frag_fav_list, container, false);
        toolbar = containerView.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main);
        Drawable drawable = toolbar.getOverflowIcon();
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), Color.WHITE);
            toolbar.setOverflowIcon(drawable);
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.about:
                        Navigation.findNavController(containerView).navigate(R.id.aboutFragment);
                        return true;
                    case R.id.logout:
                        if (Boolean.parseBoolean(getResources().getString(R.string.use_auth))) {
                            AWSMobileClient.getInstance().signOut();
                            Intent intent = new Intent(FavoritesApp.getAppContext(), AuthActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return true;
                }
                return false;
            }
        });

        //toolbar
        headerTitle = containerView.findViewById(R.id.txtHeaderTitle);
        imgSearch = containerView.findViewById(R.id.btnSearch);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // toggleMode();
                //Navigation to other frag
                Navigation.findNavController(view).navigate(R.id.googleListFragment);


            }
        });


        txtError = containerView.findViewById(R.id.txtError);
        txtNoData = containerView.findViewById(R.id.txtNoData);
        progressBar = containerView.findViewById(R.id.progressBar);
        recyclerView = containerView.findViewById(R.id.recyclerView);
        viewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
        imgSearch.setVisibility(View.VISIBLE);
        headerTitle.setText("Favorites List");
        containerView.setBackgroundColor(ContextCompat.getColor(FavoritesApp.getAppContext(), R.color.light_yellow));

        favoriteAdapter = new FavoriteAdapter(retryCallback);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(favoriteAdapter);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 10);
        recyclerView.setItemViewCacheSize(10);

        viewModel.getBooksList().observe(Objects.requireNonNull(getActivity()), new Observer<PagedList<FavoriteItem>>() {
            @Override
            public void onChanged(PagedList<FavoriteItem> articles) {
                if (articles != null) {
                    favoriteAdapter.submitList(articles);
                }
            }
        });


        txtError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.retry();
            }
        });

        viewModel.getState().observe(getActivity(), new Observer<LoadState>() {
            @Override
            public void onChanged(LoadState state) {
                MainActivity.hideKeyboardFrom(getContext(), FavoriteListFragment.this.getView());
                recyclerView.setVisibility(View.VISIBLE);

                if (viewModel.listIsEmpty() && state == LoadState.LOADING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                if (viewModel.listIsEmpty() && state == LoadState.ERROR) {
                    txtError.setVisibility(View.VISIBLE);
                } else {
                    txtError.setVisibility(View.GONE);
                }

                if (state == LoadState.NODATA) {
                    recyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                }

                if (!viewModel.listIsEmpty()) {
                    if (state == null) {
                        favoriteAdapter.setLoadState(LoadState.DONE);
                    } else {
                        favoriteAdapter.setLoadState(state);
                    }
                }
                favoriteAdapter.notifyDataSetChanged();

            }

        });

        return containerView;
    }


    RetryCallback retryCallback = new RetryCallback() {
        @Override
        public void retry() {
            viewModel.retry();
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        View rootView = getActivity().findViewById(R.id.mainRelativeLayout);
        if (null == getArguments()) return;
        String strAction = getArguments().getString(Constants.action, "no-action");
        if (!strAction.equals("no-action")) {
            Snackbar snackbar = Snackbar.make(rootView, strAction, 1000);
            View snackBarView = snackbar.getView();
            switch (strAction) {
                case "Favorite Added":
                    snackBarView.setBackgroundColor(ContextCompat.getColor(FavoritesApp.getAppContext(),R.color.green));
                    break;

                case "Favorite Removed":

                    snackBarView.setBackgroundColor(ContextCompat.getColor(FavoritesApp.getAppContext(),R.color.red));
                    break;
            }
            snackbar.show();

        }
    }
}
