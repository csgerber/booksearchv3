package com.us.broadreach.stack.AA_view.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.us.broadreach.stack.R;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        if (null == view) return;
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public Fragment getForegroundFragment(){
        Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.activity_main_navhostfragment);
        return navHostFragment == null ? null : navHostFragment.getChildFragmentManager().getFragments().get(0);
    }

    @Override
    public void onBackPressed() {

        Fragment foregroundFragment = getForegroundFragment();
        if (null == foregroundFragment) return;

        switch (foregroundFragment.getClass().getSimpleName()){
            case "GoogleListFragment":
                new AlertDialog.Builder(this)
                        .setTitle("Exit Favorites App")
                        .setMessage("Are you sure you want to exit the app?")
                        .setPositiveButton("EXIT", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finishAffinity();
                                System.exit(0);
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton("CANCEL", null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
            default:
                Navigation.findNavController(this, R.id.activity_main_navhostfragment ).navigate(R.id.googleListFragment);
                break;

        }

    }
}