package com.us.broadreach.stack.CC_model.repositories;



import com.us.broadreach.stack.CC_model.apis.FavoriteService;

import com.us.broadreach.stack.FavoritesApp;
import com.us.broadreach.stack.R;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavoriteRepository {
    public static FavoriteService getService() {


       String base_url = FavoritesApp.getAppContext().getResources().getString(R.string.base_url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(FavoriteService.class);
    }
}
